"Magnetic Scroll Template Demo" by Florian Cargoët.

Include Location Images by Emily Short.

[
 - The "Magnetic Scroll" template needs to be in the *.materials/Templates folder or in the global Inform templates folder (see documentation WI-25.13 for the location on your system).
 - For a web release with Quixe, a Python script must be executed after each release to make images available to the web game. 
    - Script: https://eblong.com/zarf/blorb/blorbtool.py
    - Command:  python blorbtool.py GAME.materials/Release/GAME.gblorb giload GAME.materials/Release/interpreter interpreter
]
Release along with an interpreter and a "Magnetic Scroll" website.

[ Images need to be in the *.materials/Figures folder. ]
Figure of lake is the file "lake.png".

Before starting the virtual machine:
	[ Give a fixed height & black background to the image window. ]
	now the scale method of the graphics window is g-fixed-size;
	now the measurement of the graphics window is 216;
	now the background color of the graphics window is "#000000";
	[ Centered image, downscaled if required. ]
	now the current graphics drawing rule is the standard placement rule;
		
The Lake is a room. 
The room-illustration is the figure of lake.
"A lake is an area filled with water, localized in a basin, surrounded by land, apart from any river or other outlet that serves to feed or drain the lake. Lakes lie on land and are not part of the ocean, although like the much larger oceans, they form part of Earth's water cycle. Lakes are distinct from lagoons which are generally coastal parts of the ocean. They are generally larger and deeper than ponds, which also lie on land, though there are no official or scientific definitions. Lakes can be contrasted with rivers or streams, which are usually flowing in a channel on land. Most lakes are fed and drained by rivers and streams."
